package parser

type MeasurementMap map[string]int

var (
	MeasurementWaterLevel     = "water_level_nap"
	MeasurementAirTemperature = "air_temperature_c"
)

// Measurements ...
type MeasurementPayload struct {
	ID    int         `json:"id,omitempty"`
	Value interface{} `json:"value,omitempty"`
}

const (
	MFM_V3              = "v3_waterlevel"
	MFM_PRESSURE_LEGACY = "pressure_legacy"
)

// Configuration ...
type DeviceConfiguration struct {
	Height    float32 `json:"height"`
	Variation string  `json:"variation"`
}
