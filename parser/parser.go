package parser

import (
	"encoding/base64"
	"encoding/binary"
	"fmt"
	"math"

	"github.com/mitchellh/mapstructure"
	"gitlab.com/sensorbucket/go-worker"
	"gitlab.com/sensorbucket/go-worker/pipeline"
)

// Parser ...
type Parser struct {
	mapping MeasurementMap
}

// New Creates a new parser
func New(mapping MeasurementMap) *Parser {
	return &Parser{
		mapping: mapping,
	}
}

func unmarshalMap(dest interface{}, data interface{}) error {
	decoder, err := mapstructure.NewDecoder(&mapstructure.DecoderConfig{
		TagName: "json",
		Result:  dest,
	})
	if err != nil {
		return err
	}
	err = decoder.Decode(data)
	if err != nil {
		return err
	}
	return nil
}

// Parse called when a new message has been received on the queue
func (p *Parser) Parse(ctx *worker.ParseContext) error {
	var err error
	var data pipeline.DevicePayload

	// Try to decode the payload
	err = unmarshalMap(&data, ctx.Message.Payload)
	if err != nil {
		return fmt.Errorf("could not map ctx.Message.Payload to DevicePayload: %w", err)
	}

	// Get MFM configuration
	var devConf DeviceConfiguration
	err = unmarshalMap(&devConf, ctx.Message.Device.Configuration.Type)
	if err != nil {
		return fmt.Errorf("could not map ctx.Message.Device.Configuration.Type to DeviceConfiguration: %w", err)
	}

	// Parse to measurements
	buf, err := base64.StdEncoding.DecodeString(data.DeviceData)
	if err != nil {
		return err
	}

	var measurements []MeasurementPayload
	// Find the corresponding variation
	switch devConf.Variation {
	case MFM_V3:
		measurements, err = parseVariationV3(p.mapping, &devConf, buf)
	case MFM_PRESSURE_LEGACY:
		measurements, err = parseVariationPressureLegacy(p.mapping, &devConf, buf)
	default:
		return fmt.Errorf("unknown MFM variation %s", devConf.Variation)
	}

	// Catch any error the switch statement above may have returned
	if err != nil {
		return err
	}

	// Send to queue
	err = ctx.Publish(pipeline.MeasurementMessage, measurements)
	if err != nil {
		return err
	}

	return ctx.Finish()
}

func parseVariationV3(mapping map[string]int, conf *DeviceConfiguration, data []byte) ([]MeasurementPayload, error) {
	measurements := []MeasurementPayload{
		{
			ID:    mapping[MeasurementWaterLevel],
			Value: conf.Height - float32(binary.LittleEndian.Uint16(data)/100.0),
		},
		{
			ID:    mapping[MeasurementAirTemperature],
			Value: math.Float32frombits(binary.LittleEndian.Uint32(data[2:])),
		},
	}
	return measurements, nil
}

func parseVariationPressureLegacy(mapping map[string]int, conf *DeviceConfiguration, data []byte) ([]MeasurementPayload, error) {
	if len(data) < 9 {
		return nil, fmt.Errorf("not enough bytes in device payload to decode")
	}

	/*
		"pressure_short", 	H (short)
		"ds18b20",					f (float)
		"bmp280_pressure"		f (float)
	*/

	// Get readout from water pressure sensor: XMLP001GD11F
	XMLP001GD11F := binary.LittleEndian.Uint16(data[0:])
	// Convert it to Bar
	waterPressure := (float32(XMLP001GD11F)-500)/4000*1000 + 1000
	// Get readout from air pressure sensor
	airPressure := math.Float32frombits(binary.LittleEndian.Uint32(data[6:]))
	// Convert to water level
	waterLevel := (waterPressure - airPressure) / 0.980638 / 1000

	// Get readout from air temperature
	airTemperature := math.Float32frombits(binary.LittleEndian.Uint32(data[2:]))

	// Build measurements
	measurements := []MeasurementPayload{
		{
			ID:    mapping[MeasurementWaterLevel],
			Value: waterLevel,
		},
		{
			ID:    mapping[MeasurementAirTemperature],
			Value: airTemperature,
		},
	}
	return measurements, nil
}
