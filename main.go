package main

import (
	"encoding/json"
	"os"

	"github.com/sirupsen/logrus"
	"gitlab.com/sensorbucket/go-mfm-worker/parser"
	"gitlab.com/sensorbucket/go-worker"
)

const (
	cfgAmqpURI            = "AMQP_URI"
	cfgAmqpQueue          = "AMQP_QUEUE"
	cfgAmqpExchange       = "AMQP_EXCHANGE"
	cfgManagementURI      = "MANAGEMENT_URI"
	cfgMeasurementUnitMap = "MEASUREMENT_UNIT_MAP"
)

func main() {
	mapping := parser.MeasurementMap{}
	err := json.Unmarshal([]byte(os.Getenv(cfgMeasurementUnitMap)), &mapping)
	if err != nil {
		logrus.WithError(err).Fatal("Could not read measurement mapping")
	}

	etl := parser.New(mapping)

	worker.Start(&worker.Config{
		AMQPURI:  os.Getenv(cfgAmqpURI),
		Queue:    os.Getenv(cfgAmqpQueue),
		Exchange: os.Getenv(cfgAmqpExchange),
		MGMTURI:  os.Getenv(cfgManagementURI),
		ETL:      etl,
	})
}
